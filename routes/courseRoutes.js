const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers")
const auth = require("../auth")

// Route for creating a course with admin verification.
router.post("/", auth.verify, (req,res) =>{

    const userData = auth.decode(req.headers.authorization);

        if(userData.isAdmin){
            courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
        }
        else{
            res.send("You don't have permission on this page!");
        }
    })

// My answer
    // router.post("/", auth.verify, (req,res) =>{
    //     courseControllers.addCourse(req.body).then(resultFromController =>{
    //         if(req.body.isAdmin !== true){
    //             res.send("You don't have permission on this page!");
    //         }
    //         else{
    //             res.send(resultFromController)
    //         }
    //     } );
    // })

// Route for retrieving all courses
router.get("/all", auth.verify, (req,res) => {
    courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all active course
router.get("/", (req, res) =>{
    courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific course
router.get("/:courseId", (req, res) =>{
    console.log(req.params.courseId);

    courseControllers.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) =>{
    // search key, //update
courseControllers.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})

// Route to archive a course
router.patch("/:courseId/archive", auth.verify, (req, res) =>{
    courseControllers.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;